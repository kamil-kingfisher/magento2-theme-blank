const gulp = require('gulp')
const sass = require('gulp-sass')
const path = require('path')

const config = {
  src: './web/scss/*.scss',
  dest: './web/css/'
}

// Compile CSS
gulp.task('styles', function () {
  var stream = gulp
    .src([config.src])
    .pipe(sass({
      includePaths: [
        path.resolve('../../'), // vendor folder if installed as a composer package
        path.resolve('../../../../../vendor/'), // vendor folder if the theme is installed manually
      ],
    }).on('error', sass.logError));

  return stream
    .pipe(gulp.dest('./web/css/'));
});
